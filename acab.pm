#lang pollen

◊subtitle{АКАБ}

◊p{Eco-anarchism  is  the  form  of  political  ecology  that  situates  the
political  most  deeply  in  Earth history and the crisis of the Earth. It can
be traced back to the work of geographer-philosopher-revolutionary Jacques
Élisée Reclus, who depicted Earth’s history as a struggle for the free flourishing
of both humanity and nature, and against the forces of domination that constrain
that flourishing. Eco-anarchism as a form of radical communitarianism has a
primary ecological commitment to promoting the flourishing of the entire global
community-of-communities, and a primary anarchic commitment to defending that
community from all destructive forces that would crush and extinguish it.}

◊q{Humanity is Nature becoming self-conscious}

◊p{Eco-anarchist politics has two major expressions. The first is direct action
to prevent the developing social-ecological catastrophe, and the second is the
struggle for a comprehensive programme for social and ecological regeneration
and the creation of a free ecological society. These two approaches are
illustrated here through the radical eco-defense organization Earth First! and
through the Sarvodaya Movement for non-violent social transformation.}