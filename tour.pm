#lang pollen

◊subtitle{Подрывной тур}

◊p{In it, while ruminating on his time as a park ranger at Utah’s Arches National
Monument (now a national park), Abbey alluded to late-night sabotage campaigns by
wilderness lovers that had begun in the late 1950s. Soon Abbey published The Monkey
Wrench Gang (1975), a novel about a band of passionate if crazed and angry
environmentalists who roamed the deserts of the Southwest, destroying billboards,
bulldozers, and conspiring to blow up Arizona’s Glen Canyon Dam to liberate the
Colorado River, which they felt had been unjustly incarcerated behind it. Abbey
combined evocative, pantheistic writing about the sublime value of nature, with a
unique form of libertarian anarchism that resonated with biocentrism, and by so doing,
inspired many of those who formed Earth First! Earlier nature writers, especially
Henry David Thoreau, John Muir, and Aldo Leopold, also helped to kindle the movement,
as did a host of writers who from the 1960s onward provided strong critiques of
mechanistic, hierarchal, patriarchal, monotheistic, agricultural-industrial-capitalist
societies, especially Rachel Carson, Paul Shepard, Louis Mumford, Lynn White Jr., and
Roderick Nash.
}