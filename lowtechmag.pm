#lang pollen 

◊subtitle{Inspiration: Low-tech Magazine}

◊p{Журнал Low-tech Magazine работает на солнечных батареях и публикует критические статьи об экологии.
   Во многом именно эстетика и концепции этого журнала послужили отправной точкой для сайта Taste the Waste}

◊illustration[#:src "circular_economy.png"]{Отрывок статьи в Low-tech Magazine про циркулярную экономику}


