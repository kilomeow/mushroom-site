#lang pollen

◊subtitle{Дебаты Букчина с Форменом}

◊p{But   what   is   our   oikos?   The   oikos is   a   kind
of  community,  and  specifically, the  kind  with  which  we
identify  as  our  home.   Eco-anarchism   is   thus   a   form
 of   communitarianism  in  the  strongest  sense  of  the  term.
 It  recognizes  that  we  are  members  of    communities    within
communities.    Our oikoi   include   the   primary   intimate
 community  of  the  family  and  small  circle  of   close friends.
 They   include   our   local   and   regional   communities,   both
human   and  more-than-human.  And  they  include,  finally, and most
importantly, the oikos  of  all oikoi,  our  global  household,  our
home  planet, Earth.}