var progressBar = document.createElement("div");
progressBar.setAttribute("id", "progress");

$(document).ready(function() {
    $("body").append(progressBar);
})

function updateProgress(p) {
    progressBar.style.width = "" + (window.innerWidth * p) + "px";
}

$(window).scroll(function() {
    var scrollPos = $(document).scrollTop() + window.innerHeight;
    var pageOffset = $("#flow").offset().top
    var total = $("#flow").height()
    var progress = Math.min(Math.max(scrollPos-pageOffset, 0), total)
    updateProgress(progress/total);
});
