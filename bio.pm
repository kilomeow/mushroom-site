#lang pollen

◊subtitle{Биоцентризм противостоит капитализму}

◊p{Eco-anarchism is the form of political ecology that situates  the
 political   most  deeply  in  Earth  history  and  in  the crisis 
of  the  Earth.  It  holds  that  both  our own future and the future
of the planet depend on our ability to fulfil our destiny as   a 
means   through   which   the   Earth   thinks  and  acts  for  the 
common  good  of  all  beings.  This  is  the  vision  developed  by 
the  19th  century  French  geographer  and  philosopher  Jacques
Élisée  Reclus  (1830–1905), the founder of modern eco-anarchist thought
(Clark  and  Martin,  2013).  He  was  the  first  thinker  to  develop
in  extensive detail  the  story  of  the  Earth  as  a  struggle  for
the free flourishing of both humanity and   nature,   and   against 
the   forces   of   domination that constrain that flourishing. This is
the vision that is carried on today by the eco-anarchist tradition.}