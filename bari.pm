#lang pollen

◊subtitle{Бари и борьба за права рабочих}

◊p{Earth First! is a radical environmental advocacy group[1] that emerged in the Southwestern
United States in 1979. It was founded on April 4, 1980,[2] by Dave Foreman, Mike Roselle, Howie
Wolke, Bart Koehler, and Ron Kezar.[3]}

◊p{Today there are Earth First! groups in Australia, Belgium, Canada, the Czech Republic, France,
Germany, India, Ireland, Italy, Mexico, the Netherlands, Nigeria, New Zealand, the Philippines,
Poland, Slovakia, Spain, the United Kingdom, and the United States.[4] Inspired by several
environmental writings, including Rachel Carson's Silent Spring, Aldo Leopold's land ethic, and
Edward Abbey's The Monkey Wrench Gang, a small group of environmental activists composed of Dave
Foreman, ex-Yippie Mike Roselle, Wyoming Wilderness Society representatives Bart Koehler and Howie
Wolke, and Bureau of Land Management employee Ron Kezar, united to form Earth First!. While
traveling in Foreman's VW bus from the El}

◊q{Условная цитата как подобает верстке длинных текстов ◊br{}
"No compromise in defense of Mother Earth!"}

◊p{The event that precipitated the formation of Earth First! was a devastating defeat in the late
1970s at the end of the US Forest Service’s “Roadless Area Review and Evaluation” process, in which
the Forest Service refused to grant the designation of “wilderness” to areas that many
conservationists considered biologically important. But the seeds of radical environmentalism had
already sprouted long before then. As early as the 1950s there were scattered incidents of sabotage
in the United States in defiance of environmentally destructive and aesthetically displeasing
commercial enterprises. Some of these were reflected in the writings of the Southwestern writer
Edward Abbey, first subtly, in his classic memoir Desert Solitaire (1968).
}
