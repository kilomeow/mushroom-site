function loadedNavigation () {
    var nav = document.getElementById('navigation');
    var contents = document.getElementsByClassName('contents')[0].cloneNode(true);
    nav.insertBefore(contents, nav.children[1]);
}

function showNavigation () {
    console.log("brbr");
    var nav = document.getElementById('navigation');
    nav.style.visibility = 'visible';
}

function isAnyPartOfElementInViewportVert(el) {
    // http://stackoverflow.com/questions/123999/how-to-tell-if-a-dom-element-is-visible-in-the-current-viewport
    const rect = el.getBoundingClientRect();
    const windowHeight = (window.innerHeight || document.documentElement.clientHeight);

    // http://stackoverflow.com/questions/325933/determine-whether-two-date-ranges-overlap
    return (rect.top <= windowHeight) && ((rect.top + rect.height) >= 0);
}

function someOfElementsInViewportVert (els) {
    return Array.from(els).some(isAnyPartOfElementInViewportVert)
}

function visibleCallback () {
    return isAnyPartOfElementInViewportVert(document.getElementById('about').getElementsByClassName('contents')[0]);
}

function onVisible (visible_cb, callback) {
    var old_visible;
    return function () {
        var visible = visible_cb();
        if (visible && visible != old_visible) { callback();}
        old_visible = visible;
    }
}

function onNotVisible (visible_cb, callback) {
    var old_visible;
    return function () {
        var visible = visible_cb();
        if (!visible && visible != old_visible) { callback();}
        old_visible = visible;
    }
}

function showNavigation () {
    var nav = document.getElementById('navigation');
    nav.style.visibility = 'visible';
    nav.style.opacity = 1;
}

function hideNavigation () {
    var nav = document.getElementById('navigation');
    nav.style.visibility = "hidden";
    nav.style.opacity = 0;
}

document.addEventListener("DOMContentLoaded", loadedNavigation);

addEventListener('scroll', onVisible(visibleCallback, hideNavigation), false);
addEventListener('scroll', onNotVisible(visibleCallback, showNavigation), false);
